package edu.bu.ec504.hw3.Tests.TestExtendedBST;

import edu.bu.ec504.hw3.bst.BST.BSTRotation;
import edu.bu.ec504.hw3.bst.BST.BSTRotation.RotationType;
import edu.bu.ec504.hw3.bst.ExtendedBSTSet;

import java.util.Arrays;

public class TestRotation {

  public static void main(String[] args) {
    ExtendedBSTSet<Integer> intSet = new ExtendedBSTSet<>();
    Integer ints[] = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    intSet.addAll(Arrays.asList(ints));
    System.out.println("Original tree:\n"+intSet);

    for (int ii=0; ii<10; ii++)
      intSet.rotate(new BSTRotation<>(ii, RotationType.ZAG));
    System.out.println("Tree after ZAG on each node:\n"+intSet);
    if (intSet.getKey()!=9)
      throw new RuntimeException("ZAG rotations did not produce the correct root.");

    for (int ii=9; ii>=0; ii--)
      intSet.rotate(new BSTRotation<>(ii, RotationType.ZIG));
    System.out.println("Tree after ZIG on each node, in reverse order:\n"+intSet);
    if (intSet.getKey()!=0)
      throw new RuntimeException("ZIG rotations did not produce the correct root.");
  }
}
